## [1.5.0] - 2021-01-05
### Changed
- Summary is now merged with a description.
- nl2br in summary

## [1.4.0] - 2020-12-30
### Added
- Support for if calls

## [1.3.0] - 2020-12-30
### Added
- Hooks from class function
### Changed
- HTML renderer: li->h2

## [1.2.0] - 2020-12-28
### Added
- Hooks from anonymous function
- Hooks as return expression
- Hooks from simple functions without class
- @ignore tag
- @deprecated tag
- @since tag
### Changed
- Much nicer hook html
### Fixed
- Better return type info

## [1.1.0] - 2020-12-24
### Added
- Support for PHP classes

## [1.0.0] - 2020-12-23
### Added
- First version