# Hook Parser

A library to parse the plugin code to create hook documentation.

## How to document

Use PHP Doc tags when commenting *apply_filter* or *do_action* function.

### Supported tags
* @param 
* @return
* @see
* @ignore 
* @internal
* @since
* @deprecated

### Worth to know
* Parser can parse PHP code so the hooks commented out will be ignored.
* Parse errors in code will prevent from successful scanning.
* When a hook appears more than once then it will be shown in the documentation more than once. To prevent this use @ignore tag.

### Documented hook example
```php
<?php
/**
 * Do actions when shipment is created via checkout.
 *
 * @param \WPDesk_Flexible_Shipping_Shipment $shipment Created shipment.
 * @see https://flexibleshipping.com/
 * @deprecated
 */
do_action( 'flexible_shipping_checkout_shipment_created', $shipment );
```

```php
<?php

$some_bool =
    /**
     * Long text.
     * 
     * @param bool $some_bool Some bool value passed to filter.
     * @param string $some_string Some string passed to filter.
     * @return bool Returns true when in a mood. 
     * 
     * @since 1.2.3 
     * @internal Can be changed anytime. 
     */ 
    apply_filters( sprintf( 'some_filter_%s', $var), true, $someothervar );

```

## Usage

```php
<?php
$parser       = new \HookParser\CodeParser();
$renderer     = new \HookParser\HtmlRenderer();
$fileIterator = new \HookParser\PHPFileFinder();

$output = '<ul>';
foreach ( $fileIterator->getIterator( 'path/to/plugin/src' ) as $filePath ) {
    foreach ( $parser->parse( file_get_contents( current( $filePath ) ) ) as $hook ) {
        $output .= $renderer->render( $hook );
    }
}

return $output . '</ul>';
```