<?php


use HookParser\CodeParser;
use HookParser\Dto\Parameter;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{
    public function testActionParse(): void
    {
        $code = <<<'CODE'
<?php
/**
 * Do actions when shipment is created via checkout.
 *
 * @param WPDesk_Flexible_Shipping_Shipment $shipment Created shipment.
 */
do_action( 'flexible_shipping_checkout_shipment_created', $shipment );
CODE;

        $parser = new CodeParser();
        self::assertCount(1, iterator_to_array($parser->parse($code)));
        $hook = $parser->parse($code)->current();
        self::assertEquals('flexible_shipping_checkout_shipment_created', $hook->name);
        self::assertEquals('Do actions when shipment is created via checkout.', $hook->summary);

        self::assertCount(1, $hook->parameters);
        /** @var Parameter $param */
        $param = $hook->parameters[0];
        self::assertEquals('shipment', $param->name);
        self::assertEquals('\WPDesk_Flexible_Shipping_Shipment', $param->type);
        self::assertEquals('Created shipment.', $param->description);
        self::assertEquals(null, $hook->returnInfo);
        self::assertNull($hook->see);
        self::assertNull($hook->internal);
    }

    public function testFilterParse(): void
    {
        $code = <<<'CODE'
<?php
/**
 * Some test text.
 *
 * @return bool returns some values.
 */
apply_filters( 'some_filter', true );
CODE;

        $parser = new CodeParser();
        self::assertCount(1, iterator_to_array($parser->parse($code)));
        $hook = $parser->parse($code)->current();
        self::assertEquals('some_filter', $hook->name);
        self::assertEquals('Some test text.', $hook->summary);

        self::assertCount(0, $hook->parameters);
        self::assertEquals('bool returns some values.', $hook->returnInfo);
        self::assertNull($hook->see);
        self::assertNull($hook->internal);
    }

    public function testComplexName(): void
    {
        $code = <<<'CODE'
<?php
apply_filters( sprintf( 'some_filter_%s', $somevar), true, $someothervar );
CODE;

        $parser = new CodeParser();
        self::assertCount(1, iterator_to_array($parser->parse($code)));
        $hook = $parser->parse($code)->current();
        self::assertEquals('some_filter_%s', $hook->name);
        self::assertEquals('', $hook->summary);

        self::assertCount(0, $hook->parameters);
        self::assertEquals(null, $hook->returnInfo);
    }

    public function testAssignFilterExpression(): void
    {
        $code = <<<'CODE'
<?php
$variable = apply_filters( sprintf( 'some_filter_%s', $somevar), true, $someothervar );
CODE;

        $parser = new CodeParser();
        self::assertCount(1, iterator_to_array($parser->parse($code)));
        $hook = $parser->parse($code)->current();
        self::assertEquals('some_filter_%s', $hook->name);
        self::assertEquals('', $hook->summary);

        self::assertCount(0, $hook->parameters);
        self::assertEquals(null, $hook->returnInfo);
    }

    public function testUseFilterExpression(): void
    {
        $code = <<<'CODE'
<?php
$some_var = someotherfunction(some_function(
    /**
     * Some summary.
     */
    apply_filters( sprintf( 'some_filter_%s', $somevar), true, $someothervar )
));
CODE;

        $parser = new CodeParser();
        self::assertCount(1, iterator_to_array($parser->parse($code)));
        $hook = $parser->parse($code)->current();
        self::assertEquals('some_filter_%s', $hook->name);
        self::assertEquals('Some summary.', $hook->summary);

        self::assertCount(0, $hook->parameters);
        self::assertEquals(null, $hook->returnInfo);
    }

    public function testDoNotSupportVarName(): void
    {
        $code = <<<'CODE'
<?php
apply_filters( $varname, true, $someothervar );
CODE;

        $parser = new CodeParser();
        self::assertCount(0, iterator_to_array($parser->parse($code)));
    }

    public function testEmptyCodeNoException(): void
    {
        $parser = new CodeParser();
        self::assertCount(0, iterator_to_array($parser->parse('')));
    }

    public function testInternal(): void
    {
        $code = <<<'CODE'
<?php
/**
* @internal Only for internal use.
*/
apply_filters( 'name' );
CODE;

        $parser = new CodeParser();
        $hook = $parser->parse($code)->current();
        self::assertEquals('Only for internal use.', $hook->internal);
    }

    public function testSee(): void
    {
        $code = <<<'CODE'
<?php
/**
* @see https://wpdesk.net Check some url.
*/
apply_filters( 'name' );
CODE;

        $parser = new CodeParser();
        $hook = $parser->parse($code)->current();
        self::assertEquals('https://wpdesk.net Check some url.', $hook->see);
    }

    public function testIgnore(): void
    {
        $code = <<<'CODE'
<?php
/**
* @ignore
*/
apply_filters( 'name' );
CODE;

        $parser = new CodeParser();
        $hook = $parser->parse($code)->current();
        self::assertTrue($hook->ignore);
    }

    public function testReturnedFilter(): void
    {
        $code = <<<'CODE'
<?php
function some_function() {
    return apply_filters( 'hook_name' );
}
CODE;

        $parser = new CodeParser();
        $hook = $parser->parse($code)->current();
        self::assertEquals('hook_name', $hook->name);
    }

    public function testReturnedFromAnonymousFilter(): void
    {
        $code = <<<'CODE'
<?php
some_function('args', function() {
    return apply_filters( 'hook_name' );
});
CODE;

        $parser = new CodeParser();
        $hook = $parser->parse($code)->current();
        self::assertEquals('hook_name', $hook->name);
    }

    public function testActionInComplexAnonymousFunction(): void
    {
        $code = <<<'CODE'
<?php
class Plugin {
    public function hooks() {
        add_action( 'woocommerce_init', function () {
            // should be called BEFORE initialize_active_woocommerce_automations so the dependant plugins could attach themselves
            do_action(
                'shopmagic/core/initialized/v2',
                new ExternalPluginsAccess(
                    $this->plugin_info->get_version(),
                    $customer_factory,
                    $customer_interceptor,
                    $logger
                )
            );
        });
    }
}
CODE;

        $parser = new CodeParser();
        $hook = $parser->parse($code)->current();
        self::assertEquals('shopmagic/core/initialized/v2', $hook->name);
    }

    public function testDeprecated(): void
    {
        $code = <<<'CODE'
<?php
/**
* @deprecated because
*/
apply_filters( 'name' );
CODE;

        $parser = new CodeParser();
        $hook = $parser->parse($code)->current();
        self::assertEquals('because', $hook->deprecated);
    }

    public function testSince(): void
    {
        $code = <<<'CODE'
<?php
/**
* @since 1.2.3
*/
apply_filters( 'name' );
CODE;

        $parser = new CodeParser();
        $hook = $parser->parse($code)->current();
        self::assertEquals('1.2.3', $hook->since);
    }

    public function testReturnedFromClassFunction(): void
    {
        $code = <<<'CODE'
<?php
class FilterFactoryCore  {
    public function get_filter_list(): array {
        return apply_filters( 'shopmagic/core/filters', $this->get_build_in_filters() );
    }
}
CODE;

        $parser = new CodeParser();
        $hook = $parser->parse($code)->current();
        self::assertEquals('shopmagic/core/filters', $hook->name);
    }

    public function testIfStatements(): void
    {
        $code = <<<'CODE'
<?php
final class Settings {
    public function save_settings_action() {
        if ( true ) {
            do_action('shopmagic/core/settings/tabs/saved', $tab, $data_container);
        }
    }
}
CODE;

        $parser = new CodeParser();
        $hook = $parser->parse($code)->current();
        self::assertEquals('shopmagic/core/settings/tabs/saved', $hook->name);
    }
}
