<?php

namespace WPDesk\ShopMagic\Admin\Settings;

use ShopMagicVendor\WPDesk\Forms\Resolver\DefaultFormFieldResolver;
use ShopMagicVendor\WPDesk\Notice\Notice;
use ShopMagicVendor\WPDesk\Persistence\Adapter\WordPress\WordpressOptionsContainer;
use ShopMagicVendor\WPDesk\Persistence\PersistentContainer;
use ShopMagicVendor\WPDesk\View\Renderer\SimplePhpRenderer;
use ShopMagicVendor\WPDesk\View\Resolver\ChainResolver;
use ShopMagicVendor\WPDesk\View\Resolver\DirResolver;
use WPDesk\ShopMagic\Automation\AutomationPostType;
use WPDesk\ShopMagic\Helper\WordPressPluggableHelper;
use WPDesk\ShopMagic\Integration;

/**
 * Adds settings to the menu and manages how and what is shown on the settings page.
 *
 * @package WPDesk\ShopMagic\Admin\Settings
 */
final class Settings {

    /**
     * Save POST tab data. Before render.
     *
     * @return void
     */
    public function save_settings_action() {
        if ( ! empty( $_POST ) && isset( $_POST[ $tab::get_tab_slug() ] ) ) {
            do_action('shopmagic/core/settings/tabs/saved', $tab, $data_container);
        }
    }
}
