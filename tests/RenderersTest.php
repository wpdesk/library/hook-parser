<?php


use HookParser\CodeParser;
use HookParser\HtmlRenderer;
use HookParser\MarkdownRenderer;
use PHPUnit\Framework\TestCase;

class RenderersTest extends TestCase
{
    public function testMarkdownRender(): void
    {
        $code = <<<'CODE'
<?php
/**
 * Do actions when shipment is created via checkout.
 *
 * @param WPDesk_Flexible_Shipping_Shipment $shipment Created shipment.
 */
do_action( 'flexible_shipping_checkout_shipment_created', $shipment );
CODE;
        $parser = new CodeParser();
        $renderer = new MarkdownRenderer();
        $output = '';
        foreach ($parser->parse($code) as $hook) {
            $output .= $renderer->render($hook);
        }
        self::assertStringContainsString('flexible_shipping_checkout_shipment_created', $output);
    }

    public function testHTMLRender(): void
    {
        $code = <<<'CODE'
<?php
/**
 * Do actions when shipment is created via checkout.
 *
 * @param WPDesk_Flexible_Shipping_Shipment $shipment Created shipment.
 */
do_action( 'flexible_shipping_checkout_shipment_created', $shipment );
CODE;
        $parser = new CodeParser();
        $renderer = new HtmlRenderer();
        $output = '';
        foreach ($parser->parse($code) as $hook) {
            $output .= $renderer->render($hook);
        }
        self::assertStringContainsString('flexible_shipping_checkout_shipment_created', $output);
    }
}
