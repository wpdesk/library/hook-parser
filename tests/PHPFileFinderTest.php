<?php


use HookParser\PHPFileFinder;
use PHPUnit\Framework\TestCase;

class PHPFileFinderTest extends TestCase
{
    public function testCanFindAllFiles(): void
    {
        $finder = new PHPFileFinder();
        $iterator = $finder->getIterator(__DIR__ . DIRECTORY_SEPARATOR . 'testcases');
        self::assertEquals(3, iterator_count($iterator), "We should have 3 php files in testcases");
    }
}
