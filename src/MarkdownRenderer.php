<?php


namespace HookParser;


use HookParser\Dto\Hook;
use HookParser\Dto\Parameter;

final class MarkdownRenderer implements Renderer
{
    public function render(Hook $hook): string
    {
        $output = "##{$hook->name}
**{$hook->summary}**";
        if ($hook->returnInfo !== null) {
            $output .= 'Returns: ' . $hook->returnInfo;
        }

        $output .= "
Parameters:
";
        foreach ($hook->parameters as $param) {
            /** @var Parameter $param */
            $output .= "* {$param->type} \${$param->name} {$param->description}";
        }

        return $output;
    }
}