<?php


namespace HookParser;


use HookParser\Dto\Hook;
use HookParser\Dto\Parameter;
use JetBrains\PhpStorm\Pure;

final class HtmlRenderer implements Renderer
{
    #[Pure] public function render(Hook $hook): string
    {
        if ($hook->ignore) {
            return '';
        }
        $summary = nl2br($hook->summary);
        $output = "<h2>{$hook->name}</h2><p>{$summary}</p>";

        if (is_array($hook->parameters) && count($hook->parameters) > 0) {
            $output .= "<p>Parameters:</p><ul>";

            foreach ($hook->parameters as $param) {
                /** @var Parameter $param */
                $output .= "<li><em>{$param->type}</em> <strong>\${$param->name}</strong> <span>{$param->description}</span></li>";
            }
        }
        $output .= "</ul>";

        if ($hook->returnInfo !== null) {
            $info = explode(' ', $hook->returnInfo, 2);
            $info[1] = $info[1] ?? '';
            $output .= "<p>Returns: <em>{$info[0]}</em> {$info[1]}</p>";
        }

        if ($hook->since !== null) {
            $output .= "<p><em>Hook available since version:</em> {$hook->since}</p>";
        }

        if ($hook->internal !== null) {
            $output .= "<p><strong>Warning - only for advanced users:</strong> {$hook->internal}</p>";
        }

        if ($hook->see !== null) {
            $output .= "<p><em>Also see:</em> {$hook->see}</p>";
        }

        if ($hook->deprecated !== null) {
            $output .= "<p><em>This hook is deprecated:</em> {$hook->deprecated}</p>";
        }

        return $output;
    }
}