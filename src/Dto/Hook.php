<?php


namespace HookParser\Dto;


class Hook
{
    /**
     * Hook constructor.
     *
     * @param string $name Hook name.
     * @param bool $isAction It is an action or a filter.
     * @param string $summary Main description of the hook. If empty then ''
     * @param string|null $returnInfo Optional return type. Only filters has this.
     * @param Parameter[] $parameters
     * @param int $line Line number in php file.
     * @param string|null $internal If @internal then not null.
     * @param string|null $see If @see then not null.
     * @param bool $ignore
     */
    public function __construct(
        public string $name,
        public bool $isAction,
        public string $summary,
        public ?string $returnInfo,
        public array $parameters,
        public int $line,
        public ?string $internal,
        public ?string $see,
        public ?string $since,
        public ?string $deprecated,
        public bool $ignore = false,
    ) {
    }

    /**
     * @return string Hook name.
     */
    public function __toString(): string
    {
        return $this->name;
    }
}