<?php


namespace HookParser\Dto;


final class Parameter
{
    public function __construct(
        public string $name,
        public string $type,
        public string $description
    ) {
    }
}