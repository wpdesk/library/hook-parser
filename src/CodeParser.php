<?php


namespace HookParser;


use Generator;
use HookParser\Dto\Hook;
use HookParser\Dto\Parameter;
use phpDocumentor\Reflection\DocBlock;
use phpDocumentor\Reflection\DocBlock\Tags\Param;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use phpDocumentor\Reflection\DocBlockFactory;
use PhpParser\Node\Expr;
use PhpParser\Node\Scalar;
use PhpParser\Node\Stmt;
use PhpParser\ParserFactory;

final class CodeParser
{
    private DocBlockFactory $docBlockFactory;

    public function __construct()
    {
        $this->docBlockFactory = DocBlockFactory::createInstance();
    }

    /**
     * @param string $code
     *
     * @return Generator|Hook[]
     * @noinspection PhpDocSignatureInspection
     */
    public function parse(string $code): Generator
    {
        foreach ($this->traverseFuncCalls($code) as $expr) {
            $hookCall = $this->tryGetHookFuncCall($expr);
            if ($hookCall !== null) {
                $hook = $this->tryCreateHookFromFoundFuncCall($hookCall, true);
                if ($hook !== null) {
                    yield $hook;
                }
            }
        }
    }

    private function traverseFuncCalls(string $code): Generator
    {
        $parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
        $nodeList = $parser->parse($code) ?? [];

        /** @var Stmt $node */
        foreach ($nodeList as $node) {
            if ($node instanceof Stmt\Function_) {
                yield from $this->scanFunctionForFuncCall($node);
            }
            if ($node instanceof Stmt\Class_) {
                yield from $this->scanClassForForFuncCall($node);
            }
            if ($node instanceof Stmt\Namespace_) {
                yield from $this->scanNamespaceForFuncCall($node);
            }
            if ($node instanceof Stmt\Expression) {
                yield from $this->scanExpressionForFuncCall($node);
            }
        }
    }

    private function scanFunctionForFuncCall(Stmt\Function_ $function): Generator
    {
        yield from $this->scanStatementListForFuncCalls($function->stmts);
    }

    private function scanStatementListForFuncCalls(array $statements): Generator
    {
        foreach ($statements as $stmt) {
            if ($stmt instanceof Stmt\Class_) {
                yield from $this->scanClassForForFuncCall($stmt);
            }
            if ($stmt instanceof Stmt\If_ && is_array($stmt->stmts)) {
                yield from $this->scanStatementListForFuncCalls($stmt->stmts);
            }
            if ($stmt instanceof Stmt\Return_ && $stmt->expr instanceof Expr\FuncCall) {
                yield from $this->scanFuncForAllFuncCalls($stmt->expr);
            }
            if ($stmt instanceof Stmt\Expression) {
                yield from $this->scanExpressionForFuncCall($stmt);
            }
            if ($stmt instanceof Stmt\ClassMethod && is_array($stmt->stmts)) {
                yield from $this->scanStatementListForFuncCalls($stmt->stmts);
            }
        }
    }

    private function scanNamespaceForFuncCall(Stmt\Namespace_ $namespace): Generator
    {
        yield from $this->scanStatementListForFuncCalls($namespace->stmts);
    }

    private function scanClassForForFuncCall(Stmt\Class_ $class): Generator
    {
        yield from $this->scanStatementListForFuncCalls($class->stmts);
    }

    private function scanExpressionForFuncCall(Stmt\Expression $expr): Generator
    {
        if ($expr->expr instanceof Expr\FuncCall) {
            yield from $this->scanFuncForAllFuncCalls($expr->expr);
        }

        if ($expr->expr instanceof Expr\Assign) {
            yield from $this->scanAssignExpressionForFuncCall($expr->expr);
        }
    }

    private function scanFuncForAllFuncCalls(Expr\FuncCall $expr): Generator
    {
        yield $expr;
        foreach ($expr->args as $arg) {
            if ($arg->value instanceof Expr\FuncCall) {
                yield from $this->scanFuncForAllFuncCalls($arg->value);
            }
            if ($arg->value instanceof Expr\Closure && is_array($arg->value->stmts)) {
                yield from $this->scanStatementListForFuncCalls($arg->value->stmts);
            }
        }
    }

    private function scanAssignExpressionForFuncCall(Expr\Assign $expr): Generator
    {
        if ($expr->expr instanceof Expr\FuncCall) {
            yield from $this->scanFuncForAllFuncCalls($expr->expr);
        }
        if ($expr->expr instanceof Expr\Assign) {
            yield from $this->scanAssignExpressionForFuncCall($expr->expr);
        }
    }

    private function tryGetHookFuncCall(Expr $expr): ?Expr\FuncCall
    {
        if ($expr instanceof Expr\FuncCall) {
            $functionName = $expr->name->parts[0];
            if (in_array($functionName, ['do_action', 'apply_filters'])) {
                return $expr;
            }
        }

        return null;
    }

    private function tryCreateHookFromFoundFuncCall(Expr\FuncCall $func, bool $isAction): ?Hook
    {
        $hookName = $this->getHookName($func->args[0]->value);
        if ($hookName === '') {
            return null;
        }
        $docBlock = $this->createDocBlock($func);

        return new Hook(
            $hookName,
            $isAction,
            trim($docBlock->getSummary() . "\n" . $docBlock->getDescription()),
            $this->getReturnType($docBlock),
            $this->createParameters($docBlock),
            (int)$func->getAttribute('startLine'),
            $this->getInternal($docBlock),
            $this->getSee($docBlock),
            $this->getSince($docBlock),
            $this->getDeprecated($docBlock),
            $this->getIgnore($docBlock)
        );
    }

    private function getHookName(mixed $arg): string
    {
        if ($arg instanceof Scalar\String_) {
            return $arg->value;
        }
        if ($arg instanceof Expr\FuncCall) {
            return $this->getHookName($arg->args[0]->value);
        }

        return '';
    }

    private function createDocBlock(Expr\FuncCall $func): DocBlock
    {
        return $this->docBlockFactory->create($func->getDocComment()?->getText() ?? '/** */');
    }

    private function getReturnType(DocBlock $doc): ?string
    {
        foreach ($doc->getTagsByName('return') as $returnTag) {
            if ($returnTag instanceof Return_) {
                return (string)$returnTag;
            }
        }

        return null;
    }

    private function getInternal(DocBlock $doc): ?string
    {
        if (($internalTag = current($doc->getTagsByName('internal'))) instanceof DocBlock\Tags\Generic) {
            return (string)$internalTag;
        }

        return null;
    }

    private function getIgnore(DocBlock $doc): bool
    {
        if (current($doc->getTagsByName('ignore')) instanceof DocBlock\Tags\Generic) {
            return true;
        }

        return false;
    }

    private function getDeprecated(DocBlock $doc): ?string
    {
        if (($tag = current($doc->getTagsByName('deprecated'))) instanceof DocBlock\Tags\Deprecated) {
            return (string)$tag;
        }

        return null;
    }

    private function getSince(DocBlock $doc): ?string
    {
        if (($tag = current($doc->getTagsByName('since'))) instanceof DocBlock\Tags\Since) {
            return (string)$tag;
        }

        return null;
    }

    private function getSee(DocBlock $doc): ?string
    {
        if (($seeTag = current($doc->getTagsByName('see'))) instanceof DocBlock\Tags\See) {
            return (string)$seeTag;
        }

        return null;
    }

    private function createParameters(DocBlock $doc): array
    {
        $parameters = [];
        foreach ($doc->getTags() as $tag) {
            if ($tag instanceof Param) {
                $parameters[] = new Parameter(
                    $tag->getVariableName(),
                    (string)$tag->getType(),
                    (string)$tag->getDescription()
                );
            }
        }

        return $parameters;
    }
}