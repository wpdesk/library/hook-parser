<?php

namespace HookParser;

use HookParser\Dto\Hook;

interface Renderer
{
    public function render(Hook $hook): string;
}